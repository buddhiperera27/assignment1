#include <stdio.h>

int main(void) {
  
  char letter;

  printf( "Enter a character : " );
  scanf( "%c" , &letter );

  if ( letter == 'A' || letter == 'a' )
  {
    printf( "The character %c is a vowel.\n" , letter );
  }
  else if ( letter == 'E' || letter == 'e' )
  {
    printf( "The character %c is a vowel.\n" , letter );
  }
  else if ( letter == 'I' || letter == 'i' )
  {
    printf( "The character %c is a vowel.\n" , letter );
  }
  else if ( letter == 'O' || letter == 'o' )
  {
    printf( "The character %c is a vowel.\n" , letter );
  }
  else if ( letter == 'U' || letter == 'u' )
  {
    printf( "The character %c is a vowel.\n" , letter );
  }
  else
  {
    printf( "The character %c is a consonant.\n" , letter );
  }

  return 0;
}