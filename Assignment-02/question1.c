#include <stdio.h>

int main(void) {
  
  int num;

  printf( "Enter a Number : " );
  scanf( "%d" , &num );

  if ( num == 0 )
  {
    printf( "\nThe number is Zero\n" );
  }
  else if ( num > 0 )
  {
    printf( "\nThe number is Positive\n" );
  }
  else if ( num < 0 )
  {
    printf( "\nThe number is negetive\n" );
  }
  else
  {
    printf( "\nError! Please check the number.\n" );
  }

  return 0;
}