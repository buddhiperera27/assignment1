#include <stdio.h>

int main(void) {
  
  int num;

  printf( "Enter a Number : " );
  scanf( "%d" , &num );

  if ( num < 1 )
  {
    printf( "The number %d is not in the range\n" , num );
  }
  else if ( num % 2 == 0 )
  {
    printf( "This number is even\n" );
  }
  else if ( num % 2 == 1 )
  {
    printf( "This number is odd\n" );
  }
  
  else
  {
    printf( "\nError! Please check the number.\n" );
  }

  return 0;
}