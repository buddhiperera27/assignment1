#include <stdio.h>

int main(void)
{
  int num , tot = 0;

  printf( "Enter a number : " );
  scanf( "%d" , &num );

  while ( num > 1 )
  {
    tot = tot + num;
    
    printf( "Enter a number : " );
    scanf( "%d" , &num );
  }

  printf( "\nThe total is %d" , tot );

  return 0;
}