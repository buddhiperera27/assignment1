#include <stdio.h>
int main(void)
{
  int num , rev = 0;

  printf( "Enter a number to reverse : " );
  scanf( "%d", &num );

  while ( num != 0 )
  {
    
    rev = rev * 10;
    rev = rev + num % 10;
    num = num / 10;

  }

  printf( "The reverse number is %d\n", rev );

  return 0;
}