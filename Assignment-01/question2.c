#include <stdio.h>
//Program read a radius and computes the area of a disk.

int main(void) {
  
  float r , area , pi = 3.14;
  
  printf( "Enter the radius : " );
  scanf( "%f" , &r );

  area = pi * r * r;

  printf( "\nRadius of the circle is %.2f" , r );
  printf( "\nThe area of the circle is %.4f" , area );

  printf( "\n\n** END **\nThank You!" );

  return 0;
  
}