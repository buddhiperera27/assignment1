#include <stdio.h>
//Program to read two integers and swap it

int main(void) {
  
  int a , b , tempory; 
  
  printf( "Enter the frst number as 'a' : " );
  scanf( "%d" , &a );
  printf( "Enter the second number as 'b'  : " );
  scanf( "%d" , &b );

  printf( "\n\nThe number you enterd as 'a' is %d" , a );
  printf( "\nThe number you enterd as 'b' is %d" , b );

  tempory = a;
  a = b;
  b = tempory;

  printf( "\n\n'a' variable number after swap is %d" , a );
  printf( "\n'b' variable number after swap is %d" , b );

  printf( "\n\n** End **\nThank You!" );

  return 0;

}
